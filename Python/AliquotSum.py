def aliquotSum(n):
    sm = 0
    if (n<0):
        return 0
    for i in range(1, n):
        if (n % i == 0):
            sm = sm + i
    return sm

def classify(n):
    sm = aliquotSum(n)
    if (sm == n):
        return "Gamma=
    elif (sm < n):
        return "8Alpha"
    else:
        return "Beta"

