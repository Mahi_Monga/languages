from collections import defaultdict


def is_palindrome(string: str) -> bool:
    string = ''.join(char.lower() for char in string if char.isalnum())

    count_dict = defaultdict(int)
    for char in string:
        count_dict[char] += 1

    odd_count = 0
    for value in count_dict.values():

        if value % 2 != 0:
            odd_count += 1

    return odd_count <= 1


print(is_palindrome('palindrome'))
