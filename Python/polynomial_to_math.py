import re

superscipt_text = str.maketrans('0123456789', ('\u2070\u00b9\u00b2\u00b3\u2074\u2075\u2076\u2077\u2078\u2079'))

def sort_powers(poly_ex):
    if 'x' not in poly_ex:
        return 0
    return 1 if poly_ex[-1] == 'x' else int(poly_ex[poly_ex.index('^') + 1:])

def to_mathematicians(poly_ex):
    return ' '.join(x[:x.index('^')] + x[x.index('^')+1:].translate(superscipt_text) if '^' in x else x for x in sorted(poly_ex, key=sort_powers))

def to_mathematicians_way(poly_exp):
    return '${}$'.format(''.join(sorted(poly_exp, key=sort_powers)))

def insert_gap(latex_exp):
    return latex_exp.replace('-', ' -').replace('+', ' +')

def split_at_sign(latex_exp):
    return re.split(r'[ ](?=[+|/-])', latex_exp)

if __name__ == '__main__':
    latex_exp = input().strip('$')
    if latex_exp[0] != '-':
        latex_exp = '+' + latex_exp
    print(to_mathematicians_way(split_at_sign(insert_gap(latex_exp))))
    print(to_mathematicians(re.split(r'[ ](?=[+|/-])', latex_exp)))
