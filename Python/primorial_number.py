


def isFactorion(n):
    Max = 100000
    fact = [0] * MAX
    fact[0] = 1
    for i in range(1, MAX):
        fact[i] = i * fact[i - 1]
    org = n
    sum = 0
    while (n > 0):
        d = n % 10
        sum += fact[d]
        n = n // 10
    if (sum == org):
        return True
    return False


n = 40585
if (isFactorion(n)):
    print("Yes")
else:
    print("No")
