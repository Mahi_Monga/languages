def div_result(n: int) ->(bool,bool):
    return(n % 3 == 0, n % 5 == 0)
def fb3(n: int)->str:
    return{(true,true):"fizzbuzz",(true,false):"fizz",(false,true):"buzz",(false,false): str(n)}[div_result(n)]
