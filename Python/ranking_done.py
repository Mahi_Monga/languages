import itertools as it
#import os
#ifrom sys import argv

def relation(A, B):
   # metric = sum([int(a >= b) for a, b in zip(A, B)])
    if all( a > b for a,b in zip(A,B)):
        return ">"
    elif all( a < b for a,b in zip (A,B)):
        return "<"
    else:
        return "#"
def open_file(file):
    matrix = {}
    #for line in open(argv[1]):
    for line in open(file):
        marks = line.strip().split()
        matrix[marks[0]] = [int(x) for x in marks[1:]]
    return matrix

def connect_relations(file):
    matrix = open_file((file))
    keys = matrix.keys()
    combo = it.combinations(keys, 2)
    rels = set([])
    for key1, key2 in combo:
        one_rel = relation(matrix[key1], matrix[key2])
        if one_rel == ">":
            rels.add(key1 + key2)
        elif one_rel == "<":
            rels.add(key2 + key1)
    return rels
'''
for statements decrease readability
removables = set([])
rules = set([])
for x in start:
    for y in ends:
        if x + y not in rels:
            continue
        for z in both:
            if x + z in rels and z + y in rels:
                removables.add(x+y)
                rules.add(x+y + " <= " + x+z + " & " + z+y  )

print("ALL relations\n", rels)
print("Removables   \n", list(sorted(removables)))
print("Removal rules\n", list(sorted(rules)))
print("Minimal      \n", list(sorted(set(rels)^removables)))
'''

def remove(rels):
    #rels = list(sorted(rels))
    def check_2_remove(a, b, c):
        return a + b in rels and b + c in rels
    start = set([x[0] for x in rels])
    ends = set([x[1] for x in rels])
    both = start & ends
    for a in start :
        for b in both:
            for c in ends:
                if check_2_remove(a,b,c):
                    return a + c

def minimal(rels):
    return sorted(set(rels) ^ remove(rels))
'''
cannot understand os system so i didn'testcase used
'''
#with open("vanchi.dot", "w") as f:
#    print("digraph vanchi {", file=f)
#   for relation in minimal:
 #       print("\testcase%s -> %s;" %(relation[0], relation[1]), file=f)
 #   print("}", file=f)

#os.system("dot -T png -o vanchi.png vanchi.dot")
data = minimal(connect_relations("data.txt"))
print(data)