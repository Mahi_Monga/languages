import string


def caesar_encryption(str_in):
    letters = list(string.ascii_lowercase)
    str_out = ""
    for i in str_in:
        if i in letters:
            num = letters.index(i)
            try:
                str_out += letters[num + 3]
            except IndexError:
                num_out = num + 3 - 26
                str_out += letters[num_out]

        else:
            str_out += i
    print(str_out)
str_in = "Mahiz"
caesar_encryption(str_in)
