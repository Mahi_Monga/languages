ones = ["", "one ", "two ", "three ", "four ", "five ", "six ", "seven ", "eight ", "nine ", "ten ",
        "eleven ","twelve ", "thirteen ", "fourteen ", "fifteen ", "sixteen ", "seventeen ", "eighteen ",
        "nineteen "]
twenties = ["", "", "twenty ", "thirty ", "forty ", "fifty ", "sixty ", "seventy ", "eighty ", "ninety "]
western_num = ["", "thousand ", "million ", "billion ", "trillion ", "quadrillion ",
             "quintillion", "sextillion","septillion ", "octillion ", "nonillion ",
             "decillion ", "undecillion ", "duodecillion ", "tredecillion ",
             "quattuordecillion ", "quindecillion", "sexdecillion ", "septendecillion ",
             "octodecillion ","novemdecillion ", "vigintillion "]

def western(n):

    single_digit = n % 10
    tens_digit = ((n % 100) - single_digit) / 10
    hundreds_digit = ((n % 1000) - (tens_digit * 10) - single_digit) / 100
    t = ""
    h = ""
    if hundreds_digit != 0 and tens_digit == 0 and single_digit == 0:
        t = ones[int(hundreds_digit)] + "hundred "
    elif hundreds_digit != 0:
        t = ones[int(hundreds_digit)] + "hundred and "
    if tens_digit <= 1:
        h = ones[int(n) % 100]
    elif tens_digit > 1:
        h = twenties[int(tens_digit)] + ones[int(single_digit)]
    st = t + h
    return st


def fig2words(num):
    if num == 0:
        return 'zero'
    i = 3
    n = str(num)
    western_word = ""

    k = 0
    while i == 3:
        nw = n[-i:]
        n = n[:-i]
        if int(nw) == 0:
            western_word = western(int(nw)) + western_num[int(nw)] + western_word
        else:
            western_word = western(int(nw)) + western_num[k] + western_word
        if n == '':
            i = i + 1
        k += 1
    return western_word[:-1]

