def qsqsq() -> [int]:
    lists = []
    for i in range(1,100):
        for j in range(1,i):
            for k in range (1,i):
                if i**2 == j**3 + k**2 and i ** 2 < 1000:
                    lists.append(i**2)
    lists = list(set(lists))
    lists.sort()
    return lists