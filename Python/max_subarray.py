def max_subarray(self, nums):
    sum = 0
    min_so_far = 0
    max_sub = 0
    for element in nums:
        sum += element
        max_sub= max(sum - min_so_far, max_sub)
        min_so_far = min(min_so_far, sum)
    return max_sub
print(max_subarray([-1,2,-4,5,6])