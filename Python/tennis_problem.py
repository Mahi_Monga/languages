def match_winner(A, B):
    if A > B:
        return A
    elif B > A:
        return B


def set_winner(A, B):
    if A >= 6 and (A - B) >= 2:
        return A
    elif B >= 6 and (B - A) >= 2:
        return B


def game_winner(A, B):
    if A == 4 and B == 4:
        A, B = 3, 3
    if A >= 4 and (A - B) >= 2:
        return "A"
    elif A >= 4 and (B - A) >= 2:
        return "B"


def game(final_score):
    A, B = "A", "B"
    scores = {A: 0, B: 0}
    points = {0: 0, 1: 15, 2: 30, 3: 40, 4: "40+adv", 5: "Game_win"}
    game_winners, set_winners = [], []
    game_count, point_count = {A: [], B: []}, {A: [], B: []}
    for point in final_score:
        if point == "A":
            score = scores[A] + 1
            scores[A] = score
        elif point == "B":
            score = scores[B] + 1
            scores[B] = score
        if scores[A] >= 4 or scores[B] >= 4:
            winner = game_winner(scores[A], scores[B])
            if winner == A:
                game_winner.append(A)
            elif winner == B:
                game_winners.append(B)
            scores = {A: 0, B: 0}
    if len(game_winners) >= 6:
        s_winner = set_winner(game_winners.count(A), game_winners.count(B))
        set_winners.append(s_winner)
        game_winners = []
    game_count[A], game_count[B] = game_winners.count(A), game_winners.count(B)
    point_count[A], point_count[B] = points[scores[A]], points[scores[B]]
    return "set wins:" + str(set_winners) + "\ngame wins:" + str(game_count) + "\npoints:" + str(point_count)
