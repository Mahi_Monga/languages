def digits(n):
    return [int(ch) for ch in str(n)]


def limits_of(n):
    start = [1]
    stop = []
    start.append('0' * (int(n) - 1))
    start_num = ''.join(str(v) for v in start)
    stop.append('9' * n)
    stop_num = ''.join(str(v) for v in stop)
    required_tuple = (start_num, stop_num)
    return required_tuple


def odometer(n):
    l = []
    for i in range(int(limits_of(n)[0]), int(limits_of(n)[1]) + 1):
        d = digits(i)
        if all(i < j for i, j in zip(d, d[1:])):
            l.append(''.join(str(v) for v in d))
    return l


def term(i, f):
    f = int(f)
    n = len(i)
    l = odometer(n)
    if i in l:
        if i == l[len(l) - 1]:
            return l[f - 1]
        k = l.index(i)
        k = k + f
        return l[k]
    else:
        return ("Invalid Input")
