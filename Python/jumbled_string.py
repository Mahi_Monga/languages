def pick10(s: str, n: int) -> str:
    strng = []
    num = 0
    while len(strng) < 10:
        for i in range(1, 10):
            strng.append(s[(n + num) % len(s)])
            num += i
    return "".join(strng[:10])
