month_num = {
        jan: 1,
        feb: 2 ,
        mar: 3,
        apr: 4,
        may: 5,
        jun: 6,
        jul: 7,
        aug: 8,
        sep: 9,
        oct: 10,
        nov: 11,
        dec: 12 }


def converting_string(date):
    date = date.replace(" ", "")
    date = date.replace(",", "")
    date = date.replace(", ", "")
    return date


def day_of_year(date: str, ) -> int:
    ans = 0
    date = converting_string(date)

    year = int(date[7:-1])
    month = date[3:5]
    day = int(date[:1])

    if year % 4 == 0 and year % 100 == 0 and year % 400 == 0:
        leap = True
    elif year % 4 == 0 and year % 100 != 0:
        leap = True
    else:
        leap = False

    days_per_month = {
        jan: 31,
        feb: 28 + leap,
        mar: 31,
        apr: 30,
        may: 31,
        jun: 30,
        jul: 31,
        aug: 31,
        sep: 30,
        oct: 31,
        nov: 30,
        dec: 31
    }

    for i in range(1, month):
        ans += days_per_month[i]

    return ans + day


date = "10 jan 2012"
print(day_of_year(date))
