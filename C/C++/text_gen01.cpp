  1 #include <iostream>
  2 #include <random>
  3
  4 using namespace std;
  5
  6 const string ALPHABETS("abcdefghijklmnopqrstuvwxyz");
  7 default_random_engine rand_engine;
  8 uniform_int_distribution<size_t> dist(0, ALPHABETS.length() - 1);
  9
 10 string make_word(int how_long) {
 11         string word;
 12             for (int i = 0; i < how_long; ++i) {
 13                         word += ALPHABETS[dist(rand_engine)];
 14                             }
 15                 return word;
 16 }
 17
 18 int main(int argc, char* argv[]) {
 19
 20         for (int i = 1; i < argc; ++i) {
 21                     cout << make_word(stoi(argv[i])) << " ";
 22
 23                         }
 24             cout << endl;
 25                 return 0;
 26 }
 27
 28